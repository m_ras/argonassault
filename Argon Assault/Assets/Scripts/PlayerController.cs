﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {

    // todo work-out why we're fast af at the start

    [Header("General")]
    [Tooltip("In ms^-1")]
    [SerializeField] float xSpeed = 50f;
    [Tooltip("In ms^-1")]
    [SerializeField] float ySpeed = 50f;
    [Tooltip("Position Limit for ship on x axis ")]
    [SerializeField] float xPositionLimit = 15f;
    [Tooltip("Position Limit for ship on y axis ")]
    [SerializeField] float yPositionLimit = 6f;
    [SerializeField] GameObject[] guns;

    // Factors used for rotationg the ship
    // Position factor is where the ship is
    // Control factor is when the ship is moving (being controlled by player)
    [Header("Screen-Position Based")]
    [SerializeField] float positionPitchFactor = -2.4f;
    [SerializeField] float positionYawFactor = 2f;

    [Header("Control-throw Based")]
    [SerializeField] float controlPitchFactor = -25f;
    [SerializeField] float controlYawFactor = 25f;
    [SerializeField] float controlRollFactor = -50f;

    // Set in Translation
    float xThrow, yThrow;

    bool isControlEnabled = true;
	
	// Update is called once per frame
	void Update ()
    {
        if(isControlEnabled)
        {
            ProcessTranslation();
            ProcessRotation();
            ProcessFiring();
        }
    }

    private void ProcessTranslation()
    {
        // Calculating X Position
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        float xOffset = xThrow * xSpeed * Time.deltaTime;
        float rawNewXPos = transform.localPosition.x + xOffset;
        float newXPos = Mathf.Clamp(rawNewXPos, -xPositionLimit, xPositionLimit);

        // Calculating Y Position
        yThrow = CrossPlatformInputManager.GetAxis("Vertical");
        float yOffset = yThrow * ySpeed * Time.deltaTime;
        float rawNewYPos = transform.localPosition.y + yOffset;
        float newYPos = Mathf.Clamp(rawNewYPos, -yPositionLimit, yPositionLimit);

        // Applying Translation
        transform.localPosition = new Vector3(newXPos, newYPos, transform.localPosition.z);
    }

    private void ProcessRotation()
    {
        // Pitch is turning up/down (more like tilting)
        float pitchDueToPosition = transform.localPosition.y * positionPitchFactor;
        float pitchDueToControl = yThrow * controlPitchFactor;
        float pitch =  pitchDueToPosition + pitchDueToControl;

        // Yaw is turning left/right (more like rotating)
        float yawDueToPosition = transform.localPosition.x * positionYawFactor;
        float yawDueToControl = xThrow * controlYawFactor;
        float yaw = yawDueToPosition + yawDueToControl;

        // Roll is not in our game (spinning)
        float roll = xThrow * controlRollFactor;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void ProcessFiring()
    {
        if(CrossPlatformInputManager.GetButton("Fire"))
        {
            SetGunsActive(true);
        }
        else
        {
            SetGunsActive(false);
        }
    }

    private void SetGunsActive(bool isActive)
    {
        foreach(GameObject gun in guns)
        {
            var emissionModule = gun.GetComponent<ParticleSystem>().emission;
            emissionModule.enabled = isActive;
        }
    }

    // Called by string reference. 
    private void StopMovement()
    {
        isControlEnabled = false;
    }
}
