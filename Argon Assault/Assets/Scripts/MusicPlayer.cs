﻿using UnityEngine;

/// <summary>
/// Script relating to the splash screen logic
/// </summary>
public class MusicPlayer : MonoBehaviour {


    // Awake is called when the script instance is being loaded
    void Awake()
    {
        if(FindObjectsOfType<MusicPlayer>().Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(transform.gameObject);
        }
    }
}
