﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    int splashScreenTimer = 2;

    // Use this for initialization
    void Start()
    {
        Invoke("LoadMainMenu", splashScreenTimer);
    }

    // Load the Main Menu
    void LoadMainMenu()
    {
        SceneManager.LoadScene(1);
    }
}
