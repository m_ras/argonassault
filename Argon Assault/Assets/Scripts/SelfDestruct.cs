﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour {

    [Tooltip("In Seconds")][SerializeField] float destroyDelay = 2f;

	// Use this for initialization
	void Start () {
        print(gameObject.name);
        Destroy(gameObject, destroyDelay);
	}
}
